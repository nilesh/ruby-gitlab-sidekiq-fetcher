Source: ruby-gitlab-sidekiq-fetcher
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>,
           Sruthi Chandran <srud@debian.org>
Build-Depends: debhelper-compat (= 12),
               gem2deb,
               ruby-sidekiq (>= 5)
Standards-Version: 4.4.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-gitlab-sidekiq-fetcher.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-gitlab-sidekiq-fetcher
Homepage: https://gitlab.com/gitlab-org/sidekiq-reliable-fetch
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-gitlab-sidekiq-fetcher
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-sidekiq (>= 5),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Reliable fetch extension for Sidekiq
 This library is an extension to Sidekiq that adds support for reliable fetches
 from Redis. It's based on sidekiq-reliable-fetch.
 .
 There are two strategies implemented: Reliable fetch using `rpoplpush` command
 and semi-reliable fetch that uses regular `brpop` and `lpush` to pick the job
 and put it to working queue. The main benefit of "Reliable" strategy is that
 `rpoplpush` is atomic, eliminating a race condition in which jobs can be lost.
 .
 This package includes gitlab's fork of sidekiq-reliable-push.
